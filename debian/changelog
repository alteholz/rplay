rplay (3.3.2-20) unstable; urgency=medium

  [ Helmut Grohne ]
  * Fix FTCBFS: Use dh_auto_configure. (Closes: #1086945)
  * As a result, CFLAGS are now passed and we need to turn off
    -Werror=format-security to avoid FTBFS.

  [ Thorsten Alteholz ]
  * debian/control: remove dependency on dh-autoreconf

 -- Thorsten Alteholz <debian@alteholz.de>  Thu, 07 Nov 2024 19:22:40 +0100

rplay (3.3.2-19) unstable; urgency=medium

  * fix gcc14 issues (Closes: #1075452)
  * debian/control: Standards version moved to 4.7.0 (no changes)

 -- Thorsten Alteholz <debian@alteholz.de>  Wed, 07 Aug 2024 22:04:35 +0200

rplay (3.3.2-18) unstable; urgency=medium

  * debian/control: use dh12
  * debian/control: Standards version moved to 4.5.0 (no changes)

 -- Thorsten Alteholz <debian@alteholz.de>  Wed, 02 Dec 2020 00:04:35 +0100

rplay (3.3.2-17) sid; urgency=medium

  * debian/control: use dh11
  * debian/control: Standards version moved to 4.3.0 (no changes)
  * debian/control: add salsa VCS URLs
  * debian/rules: use hardening flags
  * Fix FTCBFS: Export a triplet-prefixed CC (Closes: #870656)
    (Thanks to Helmut Grohne for the patch)

 -- Thorsten Alteholz <debian@alteholz.de>  Sun, 13 Jan 2019 15:04:35 +0100

rplay (3.3.2-16) unstable; urgency=medium

  * debian/control: Standards version moved to 3.9.8 (no changes)
  * debian/rules: add non-empty binary-indep target in order to be
                  able to build with "dpkg-buildpackage -A"
                  thanks to Santiago Vila for the patch
                  Closes: #805959

 -- Thorsten Alteholz <debian@alteholz.de>  Fri, 15 Jul 2016 19:04:35 +0200

rplay (3.3.2-15) unstable; urgency=medium

  * debian/control: Standards version moved to 3.9.6 (no changes)
  * debian/control: use dh 9
  * debian/control: DM-Upload-Allowed: removed
  * add patch 40_clang.patch to make it compile with clang
    thanks to Nicolas Sévelin-Radiguet (Closes: #741567)
  * add README.Debian as suggested by Vincent McIntyre (Closes: #597152)

 -- Thorsten Alteholz <debian@alteholz.de>  Tue, 22 Sep 2015 22:04:35 +0200

rplay (3.3.2-14) unstable; urgency=low

  * debian/control: Standards version moved to 3.9.2 (no changes)
  * debian/control: dependency on dpatch removed
  * debian/control: add dependency on libperl4-corelibs-perl to librplay-perl
  * debian/control: architecture: all for librplay-perl
  * debian/source/format: changed to 3.0 (quilt)
  * debian/rules: dpatch removed
  * debian/rules: build-arch and build-indep according to lintian
                  recommendation added
  * debian/patches: moved from dpatch to quilt
  * debian/rplay-server.rplay: add Description: to init-script

 -- Thorsten Alteholz <debian@alteholz.de>  Wed, 14 Dec 2011 18:04:35 +0100

rplay (3.3.2-13) unstable; urgency=low

  * New maintainer (Closes: #473853)
  * patch to avoid crash in case there is nothing to play (Closes: #502610)
  * debian/copyright: copyright collected from source files (Closes: #532449)
  * debian/control: depend on libreadline-dev (Closes: #553842)
  * debian/control: renaming rplay-perl to librplay-perl (Closes: #575930)
                    according to apt-rdepends only rplay-contrib depends
                    on rplay-perl, so simply renaming should do no harm
  * debian/control: use Breaks: instead of Conflitcs: for older vresion
                    of package
  * debian/control: dependency on dpkg (>= 1.15.4) | install-info added
  * debian/control: dependency on debhelper (>= 8.0.0)
  * debian/control: standard bumped to 3.9.1
  * debian/control: new package for shared library added
                    as lintian complained about a shared library within
                    another package, the library has to be moved from
                    rplay-client to a separate package
  * debian/control: lintian overrides for symbol file added
  * debian/control: DM-Upload-Allowed added
  * debian/rules: dh_clean -k replaced by dh_prep
  * debian/compat: 5 changed to 8
  * debian/README.source added
  * debian/source/format: start with format 1.0
  * debian/*inst: change -e to set -e
  * debian/*rm: change -e to set -e
  * the following maintainer scripts have been removed (they are empty):
    - librplay3-dev.postinst
    - rplay-client.postinst

 -- Thorsten Alteholz <debian@alteholz.de>  Sat, 27 Nov 2010 16:00:00 +0100

rplay (3.3.2-12) unstable; urgency=low

  * QA upload.
  * Acknowledging NMU: thanks to Peter Eisentraut who took care
  * debian/control
    - set QA group as maintainer
    - bump Standards-Version to 3.8.0
    - added Homepage field
    - added dpatch build-dep
    - replaced ${Source-Version} with ${binary:Version} in librplay3-dev dep
    - section fixed for librplay3-dev (libdevel)
    - set section: perl for rplay-perl
    - short descriptions re-worded
  * debian/watch
    - added
  * debian/rules
    - added dpatch stuff
    - removed '-s' option from install call; thanks to Julien Danjou for the
      report; Closes: #437921
    - removed '-' in front of clean target commands
    - removed commented dh_* call
    - installed upstream changelog directly (passing the filename to
      dh_installchangelog)
    - make call are now made with $(MAKE)
    - init file installation moved from install to binary-arch target
  * debian/patches/01_previous_changes.dpatch
    - added to remove direct upstream code changes
  * debian/copyright
    - clearly separated copyright and license notices
    - indented copyright, license and upstream author with 4 spaces
    - reformat the location of a Debian system of GPLv2 license text
    - left only the caption of GPLv2 license text
  * debian/rplay-contrib.postinst
    - removed since empty
  * debian/rplay-client.postrm
    - removed since empty
  * debian/patches/10_fix_manpages.dpatch
    - added to fix hyphen-used-as-minus-sign lintian warnings
  * debian/rplay-server.rplay
    - applied the patch from Christian Perrier; thanks to him; Closes: #389067
  * debian/librplay3.docs
    - removed installation of changelog, we do it in debian/rules

 -- Sandro Tosi <matrixhasu@gmail.com>  Sun, 17 Aug 2008 23:58:00 +0200

rplay (3.3.2-11.1) unstable; urgency=low

  * Non-maintainer upload.
  * Added LSB formatted dependency info in init.d script (closes: #468571)

 -- Peter Eisentraut <petere@debian.org>  Wed, 02 Apr 2008 01:02:28 +0200

rplay (3.3.2-11) unstable; urgency=low

  * re-generated configure script (closes: 369208)

 -- lantz moore <lmoore@debian.org>  Sat, 10 Jun 2006 11:05:13 -0700

rplay (3.3.2-10) unstable; urgency=low

  * build-depends on libreadline6-dev (closes: 326302)
  * -dev depends on libc6-dev (closes: 337791)
  * documented monitor command in rptp (closes: 293899)

 -- lantz moore <lmoore@debian.org>  Sun,  1 Jan 2006 15:06:51 -0800

rplay (3.3.2-9) unstable; urgency=low

  * removed "transitional" rplay package (closes: 308703)
  * removed bogus and unused inetd decl (closes: 288437)
  * include errno.h instead of extern definition (closes: 280274)
  * no longer creates the /usr/doc link (closes: 322834)
  * applied patch for gnu and kfreebsd-gnu (closes: 267997)

 -- lantz moore <lmoore@debian.org>  Wed, 24 Aug 2005 16:56:50 -0700

rplay (3.3.2-8) unstable; urgency=low

  * better arg passing in rplaydsp. (closes: Bug#133199)
  * rplayd now closes stdin. (closes: 135672)
  * fixed redundant declarations. (closes: 138699)
  * better ogg helper.

 -- lantz moore <lmoore@tump.com>  Tue,  2 Apr 2002 17:44:19 -0800

rplay (3.3.2-7) unstable; urgency=low

  * *finally* moved librplay3-dev to development section.

 -- lantz moore <lmoore@tump.com>  Sat,  9 Feb 2002 16:14:32 -0800

rplay (3.3.2-6) unstable; urgency=low

  * make sure that we can actually write data to rplayd.  (closes: Bug#89352)
  * added some spaces to the description of the rplay package to make the
    formatting happy. (closes: Bug#123598)

 -- lantz moore <lmoore@debian.org>  Fri,  1 Feb 2002 12:16:48 -0800

rplay (3.3.2-5) unstable; urgency=low

  * rmdir /etc/rplay on purge. (closes: Bug#74397)
  * redirect std{err,out} to /dev/null in init script. (closes: Bug#120849)
  * beefed up the manpage for rplaydsp. (closes: Bug#112624)
  * fixed typo in librplay3-dev description. (closes: Bug#110478)
  * merged Marcus Brinkmann's hurd patches. (closes: Bug#77044)
  * fixed a few lintian warnings.
  * added vorbis-tools suggestion to rplay-server and helper description
    in rplay.helpers.

 -- lantz moore <lmoore@debian.org>  Thu,  6 Dec 2001 14:03:26 -0800

rplay (3.3.2-4) unstable; urgency=low

  * /etc/init.d/rplay starts at 99 and dies at 10. (closes: Bug#67203)
  * rplay-server suggested some non-existent/experimental version of
    xmp. (closes: Bug#74179)
  * /etc/rplay is completely removed upon purging. (closes: Bug#74379)
  * rplayd now uses the regex facilities from libc and no longer depends
    on librx.
  * installed default rplay.hosts.

 -- lantz moore <lmoore@debian.org>  Sat, 21 Oct 2000 15:50:33 -0700

rplay (3.3.2-3) unstable; urgency=high

  * recompile using libreadline4. (closes: Bug#63176)
  * fixed up copyright file.
  * fixed Depends lines in control file. (closes: Bug#51934)
  * added rplaydsp and moved devrplay.so to rplay-client.

 -- lantz moore <lmoore@debian.org>  Tue,  2 May 2000 14:44:43 -0700

rplay (3.3.2-2) unstable; urgency=medium

  * added soname and -D_REENTRANT (fixes 37125, 48520)
  * split package into subcomponents (fixes 36910)

 -- lantz moore <lmoore@debian.org>  Sun,  7 Nov 1999 21:20:59 -0800

rplay (3.3.2-1) unstable; urgency=low

  * new upstream version.
  * install the new devrplay library.
  * depends on perl5, closes 41473.

 -- lantz moore <lmoore@debian.org>  Sun, 18 Jul 1999 22:53:52 -0700

rplay (3.3.1-3) unstable; urgency=low

  * upstream patches fixing an annoying sprintf problem.

 -- lantz moore <lmoore@debian.org>  Wed,  4 Nov 1998 09:14:49 -0800

rplay (3.3.1-2) unstable; urgency=low

  * forgot to remove install-mime from prerm. Bug#28395

 -- lantz moore <lmoore@debian.org>  Thu, 22 Oct 1998 10:47:45 -0700

rplay (3.3.1-1) unstable; urgency=low

  * new upstream source.
  * use update-mime.

 -- lantz moore <lmoore@debian.org>  Thu, 15 Oct 1998 09:28:29 -0700

rplay (3.3.0-4) unstable; urgency=low

  * moved startup script to 40 so that rplayd gets started after amd.

 -- lantz moore <lmoore@debian.org>  Fri, 31 Jul 1998 09:30:30 -0700

rplay (3.3.0-3) unstable; urgency=low

  * applied upstream "broadcast" patch.
  * changed mpeg helper regex to match mp[1-3]
  * fixed all lintian warnings.

 -- lantz moore <lmoore@debian.org>  Wed, 29 Jul 1998 13:53:06 -0700

rplay (3.3.0-2) unstable; urgency=medium

  * fixed info installation bug.
  * fixed "corrupt installation bug" Bug#24494.
  * use the init.d and purge files for debstd.
  * remove /etc/rplay/rplay.hosts from the distribution.

 -- lantz moore <lmoore@debian.org>  Sun, 12 Jul 1998 20:39:04 -0700

rplay (3.3.0-1) unstable; urgency=low

  * Initial Release.

 -- lantz moore <lmoore@debian.org>  Mon, 22 Jun 1998 13:55:25 -0700
